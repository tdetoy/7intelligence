extends Resource

#debugging purposes
func return_json(dict):
	return JSON.print(dict)

func save_file(dict, location):
	var save_file = File.new()
	print(save_file.open(location, File.WRITE))
	save_file.store_string(JSON.print(dict))
	save_file.close()

func load_file(location):
	var load_file = File.new()
	print(load_file.open(location, File.READ))
	var string_ret = load_file.get_as_text()
	var result = JSON.parse(string_ret)
	if result.error == OK:
		return result.result
	return null
