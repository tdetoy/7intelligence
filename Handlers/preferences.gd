extends Node



var trap_color = Color(1.0, 0.0, 0.0, 1.0)


func load_preferences():
	var prefs = Main.interpreter.load_file("user://preferences.sav")
	if prefs == null:
		create_preferences()
		return
	#load preferences here
	var color_arr = prefs["trap_color"].split(",")
	trap_color = Color(color_arr[0], color_arr[1], color_arr[2])

func save_preferences(inc_values):
	trap_color = inc_values
	var prefs_dict = {}
	prefs_dict["trap_color"] = trap_color
	
	Main.interpreter.save_file(prefs_dict, "user://preferences.sav")

func create_preferences():
	var prefs_dict = {}
	
	#populate preferences file with default values
	#this will fill out as there are more values to determine
	prefs_dict["trap_color"] = Color(1.0, 0.0, 0.0, 1.0)
	
	#end values list
		
	Main.interpreter.save_file(prefs_dict, "user://preferences.sav")

func reset_preferences():
	var direct = Directory.new()
	direct.remove("user://preferences.sav")
	create_preferences()
	load_preferences()
