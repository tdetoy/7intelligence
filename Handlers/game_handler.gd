extends Node

#idea behind this handler
#any levels will be instanced underneath it and it will
#be directly responsible for switching/restarting levels
#it may also handle global audio underneath it, pending further
#structure formulation

func _ready():
	pass

func transition_level():
	pass

func recall_level():
	pass

func restart_level():
	pass

