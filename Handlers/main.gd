extends Node

onready var interpreter = load("res://Handlers/data_handler.gd").new()

var layer

func _ready():
	Preferences.load_preferences()

func report_layer(_layer):
	layer = _layer
