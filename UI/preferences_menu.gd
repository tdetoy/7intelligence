extends Control

func _ready():
	Preferences.load_preferences()
	$MarginContainer/VBoxContainer/ColorPicker.color = Preferences.trap_color

func _on_Cancel_pressed():
	get_tree().quit()


func _on_Default_pressed():
	Preferences.reset_preferences()
	$MarginContainer/VBoxContainer/ColorPicker.color = Preferences.trap_color


func _on_Save_pressed():
	Preferences.save_preferences($MarginContainer/VBoxContainer/ColorPicker.color)
#addition for merge fix
