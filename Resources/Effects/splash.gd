extends Node2D

func _ready():
	$Particles2D.emitting = true
	$Particles2D2.emitting = true

func _process(_delta):
	if(!$Particles2D.emitting && !$Particles2D2.emitting):
		queue_free()
