shader_type canvas_item;

uniform vec4 trap_color : hint_color;

float map(float val, float in_min, float in_max, float out_min, float out_max){
	return (val - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void fragment(){
	COLOR = texture(TEXTURE, UV);
	float mix_val = map(sin(TIME), -1.0, 1.0, 0.0, 0.75);
	COLOR.rgb = mix(COLOR.rgb, trap_color.rgb, mix_val);
}

