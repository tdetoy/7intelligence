shader_type canvas_item;

uniform float dof = 1.0;

void fragment(){
	COLOR = texture(SCREEN_TEXTURE, SCREEN_UV, dof);
}