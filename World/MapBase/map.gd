extends Node2D

signal obstacles_updated(obstacle_list)
signal next_level

onready var map = $Maps/TileMap
onready var trigger_parent = $Triggers
onready var event_parent = $Events
onready var actor_parent = $Actors
onready var prop_parent = $Props


var triggers = Array()
var goals = Array()
var events = Array()
var actors = Array()
var props = Array()
var spawners = Array()

var knight
#obstacles becomes a dictionary
#structure:
#obstacles[gridpos] = exceptions
#actors hold obstacles as an array
#if they belong to a group that a gridposition has in exceptions
#they do not add it to their array
var obstacles = {}
var camera_positions = []
var valid_cells = {}
var layer

class ElementSorter:
	static func id_sort(a, b):
		return a.trigger_id < b.trigger_id


func _ready():
	randomize_floor_tiles()
	var tileset = map.tile_set
	
	for tile in map.get_used_cells():
		if(tileset.tile_get_light_occluder(map.get_cellv(tile)) != null):
			var occluder_shape = tileset.tile_get_light_occluder(map.get_cellv(tile)).polygon
			var vision_occluder = Area2D.new()
			var occluder_area = CollisionShape2D.new()
			map.add_child(vision_occluder)
			vision_occluder.position = map.map_to_world(tile)
			occluder_area.shape = ConvexPolygonShape2D.new()
			occluder_area.shape.points = occluder_shape
			vision_occluder.add_child(occluder_area)
			vision_occluder.add_to_group("Blocker")
			vision_occluder.owner = map
			var vision_occluder_body = StaticBody2D.new()
			var body_area = CollisionShape2D.new()
			map.add_child(vision_occluder_body)
			vision_occluder_body.position = vision_occluder.position
			vision_occluder_body.add_child(body_area)
			body_area.shape = ConvexPolygonShape2D.new()
			body_area.shape.points = occluder_shape
		if(tileset.tile_get_shape(map.get_cellv(tile), 0) != null):
			obstacles[tile] = Array()
		valid_cells[tile] = 1.0

	events = event_parent.get_children()
	for child in trigger_parent.get_children():
		if(child.map_goal):
			goals.append(child)
		else:
			triggers.append(child)

	if(events.size() > 1):
		events.sort_custom(ElementSorter, "id_sort")
	if(triggers.size() > 1):
		triggers.sort_custom(ElementSorter, "id_sort")
	if(goals.size() > 1):
		goals.sort_custom(ElementSorter, "id_sort")
	
	for event in events:
		event.init(self)

	props = prop_parent.get_children()
	
	for prop in props:
		if(prop.is_in_group("Spawner")):
			spawners.append(prop)
	
	for trigger in triggers:
		trigger.init(self)
		for event in events:
			if(trigger.trigger_id == event.trigger_id):
				trigger.connect("trigger_sent", event, "event_triggered")

	#this is the reverse of what i would normally anticipate
	#but this is done to continually update the actor gridposition
	#for easy referencing
	#normally i would reference actors by their gridposition but
	#deleting and recreating dictionary entries would quickly get exhausting

func init(_knight, _layer):
	knight = _knight
	layer = _layer
	connect("next_level", layer, "on_next_level")

	actors = actor_parent.get_children()
	props = prop_parent.get_children()
	
	for prop in props:
		prop.init(self, map, valid_cells, obstacles, actors, props)
		
	for actor in actors:
		connect("obstacles_updated", actor, "update_obstacles")
		if(actor == knight && knight.initialized):
			actor.reinit(self, map, valid_cells, obstacles, actors, props)
		else:
			actor.init(self, map, valid_cells, obstacles, actors, props)


func get_next_map_goal():
	if(goals.size() == 0):
		emit_signal("next_level")
		for prop in props:
			if(is_instance_valid(prop)):
				prop.queue_free()
		for actor in actors:
			if(actor != knight && is_instance_valid(actor)):
				actor.queue_free()
		for trigger in triggers:
			if(is_instance_valid(trigger)):
				trigger.queue_free()
		for event in events:
			if(is_instance_valid(event)):
				event.queue_free()
		for spawner in spawners:
			if(is_instance_valid(spawner)):
				spawner.queue_free()
		for goal in goals:
			if(is_instance_valid(goal)):
				goal.queue_free()
		return
	return map.world_to_map(goals.front().position)

func register_goal_reached(gridpos):
	if(map.world_to_map(goals.front().position) == gridpos):
		goals.pop_front()

func query_map(potential_arr, criterion):
	var set = []
	
	var check_map
	
	if criterion == "Minecarts":
		check_map = $Maps/Minecarts
	
	for potential in potential_arr:
		if(check_map.get_cellv(check_map.world_to_map(map.map_to_world(potential))) != -1):
			set.append(check_map.world_to_map(map.map_to_world(potential)))
	
	return set

func randomize_floor_tiles():
	var tiles = map.get_used_cells_by_id(27)
	randomize()
	for tile in tiles:
		var new_tile
		match randi() % 7:
			0:
				new_tile = 10
			1:
				new_tile = 11
			2:
				new_tile = 12
			3:
				new_tile = 15
			4:
				new_tile = 18
			5:
				new_tile = 19
			6:
				new_tile = 21
		map.set_cellv(tile, new_tile)
	


func register_obstacle(gridpos, append, exceptions = Array()):
	if(gridpos == Vector2.INF):
		print("invalid obstacle to append/remove!")
	if(append):
		obstacles[gridpos] = exceptions
	elif(!append):
		obstacles.erase(gridpos)
	emit_signal("obstacles_updated", obstacles)

func get_goals():
	return goals

func update_map_weights(gridpos, weight):
	print("updatin")
	valid_cells[gridpos] = weight
	for actor in actors:
		actor.update_nav(valid_cells, obstacles)

func remove_object(object):
	if(props.has(object)):
		props.remove(props.find(object))
	elif(actors.has(object) != -1):
		actors.remove(props.find(object))

func update_trigger_targets(object):
	for trigger in triggers:
		trigger.update_triggers(object)
