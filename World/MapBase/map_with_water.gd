extends "map.gd"

var splash = load("res://Resources/Effects/Splash.tscn")

onready var water = $Visuals/Sprite

func _input(_event):
	if Input.is_mouse_button_pressed(BUTTON_LEFT):
		var splash_instance = splash.instance()
		water.add_child(splash_instance)
		splash_instance.global_position = water.get_global_mouse_position()
