extends Node2D

onready var movement_map = $MasterMaps/Movement
onready var minecart_map = $MasterMaps/Minecarts
onready var camera = $Camera2D
onready var position_tween = $CameraTween
onready var zoom_tween = $CameraZoomTween

var layer_path = "res://World/MapBase/Layer1/"

var level_paths = []
var cell_list = {}
var camera_positions = []
var obstacle_list = []
var level_list = []
var knight
export(int) var level_ind = 0

func _ready():
	Main.report_layer(self)
	assert($Levels.get_children().size() <= $CameraPositions.get_children().size())
	for ind in $Levels.get_children().size():
		level_paths.append(layer_path + $Levels.get_children()[ind].name + ".tscn")
	print(level_paths)
	level_list = $Levels.get_children()
	camera_positions = $CameraPositions.get_children()
	knight = load("res://Objects/Actors/Knight.tscn").instance()
	level_transition(level_ind)

func _unhandled_input(_event):
	if(Input.is_action_just_pressed("restart_level")):
		restart(level_ind)

func level_transition(ind):
	assert(level_list.size() > ind)
	
	var level = level_list[ind]
	
	if(knight.is_inside_tree()):
		level_list[ind - 1].actor_parent.remove_child(knight)
		knight.deactivate()
	
	level.actor_parent.add_child(knight)
	knight.position = level.map.map_to_world(level.map.world_to_map(level.get_goals()[0].position))
	level.init(knight, self)
	get_tree().paused = true
	zoom_tween.interpolate_property(camera,"zoom", camera.zoom, camera_positions[ind].zoom, 0.5, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	position_tween.interpolate_property(camera, "position", camera.position, camera_positions[ind].position, 0.5, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	position_tween.start()
	zoom_tween.start()

func on_next_level():
	level_ind += 1
	level_transition(level_ind)

func _on_CameraTween_tween_all_completed():
	get_tree().paused = false
	knight.activate()

func restart(ind):
	var level = level_list[ind]
	if(is_instance_valid(knight)):
		knight.queue_free()
	knight = load("res://Objects/Actors/Knight.tscn").instance()
	var level_pos = level_list[ind].position
	level.queue_free()
	level_list[ind] = load(level_paths[ind]).instance()
	$Levels.add_child(level_list[ind])
	level_list[ind].position = level_pos
	level_transition(ind)
