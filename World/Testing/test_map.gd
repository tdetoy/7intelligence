extends Node2D

var splash = load("res://Resources/Effects/Splash.tscn")

onready var map = $Maps/TileMap
onready var trigger_parent = $Triggers
onready var event_parent = $Events
onready var actor_parent = $Actors
onready var prop_parent = $Props
onready var water = $Visuals/Sprite

var triggers = Array()
var goals = Array()
var events = Array()
var actors = Array()
var props = Array()

class ElementSorter:
	static func id_sort(a, b):
		return a.trigger_id < b.trigger_id


func _ready():
	var valid_cells = []
	var obstacles = []
	var tileset = map.tile_set
	for tile in map.get_used_cells():
		if(tileset.tile_get_light_occluder(map.get_cellv(tile)) != null):
			var vision_occluder = Area2D.new()
			var occluder_area = CollisionShape2D.new()
			map.add_child(vision_occluder)
			vision_occluder.position = map.map_to_world(tile) + Vector2(8, 8)
			occluder_area.shape = RectangleShape2D.new()
			occluder_area.shape.extents = Vector2(24, 24)
			vision_occluder.add_child(occluder_area)
			vision_occluder.add_to_group("Blocker")
			vision_occluder.owner = map
			var vision_occluder_body = StaticBody2D.new()
			var body_area = CollisionShape2D.new()
			map.add_child(vision_occluder_body)
			vision_occluder_body.position = vision_occluder.position
			vision_occluder_body.add_child(body_area)
			body_area.shape = RectangleShape2D.new()
			body_area.shape.extents = Vector2(24,24)
		if(tileset.tile_get_shape(map.get_cellv(tile), 0) != null):
			obstacles.append(tile)
		valid_cells.append(tile)
	
	actors = actor_parent.get_children()
	props = prop_parent.get_children()
	
	for prop in props:
		prop.init(map.world_to_map(prop.position), map, valid_cells)
	
	for actor in actors:
		actor.init(self, map, valid_cells, obstacles, actors, props)
	
	events = event_parent.get_children()
	for child in trigger_parent.get_children():
		if(child.map_goal):
			goals.append(child)
		else:
			triggers.append(child)
	
	if(events.size() > 1):
		events.sort_custom(ElementSorter, "id_sort")
	if(triggers.size() > 1):
		triggers.sort_custom(ElementSorter, "id_sort")
	if(goals.size() > 1):
		goals.sort_custom(ElementSorter, "id_sort")
	for trigger in triggers:
		for event in events:
			if(trigger.trigger_id == event.trigger_id):
				trigger.connect("trigger_sent", event, "event_triggered")
	
	#this is the reverse of what i would normally anticipate
	#but this is done to continually update the actor gridposition
	#for easy referencing
	#normally i would reference actors by their gridposition but
	#deleting and recreating dictionary entries would quickly get exhausting

func _input(_event):
	if Input.is_mouse_button_pressed(BUTTON_LEFT):
		var splash_instance = splash.instance()
		water.add_child(splash_instance)
		splash_instance.global_position = water.get_global_mouse_position()

func get_next_map_goal():
	if(goals.size() == 0):
		return Vector2.INF
	return map.world_to_map(goals.front().position)

func register_goal_reached(gridpos):
	if(map.world_to_map(goals.front().position) == gridpos):
		goals.pop_front()

func query_map(potential_arr, criterion):
	var set = []
	
	for potential in potential_arr:
		#if potential fits criterion
		#add to set array
		pass
	
	return set


func _on_CameraTween_tween_all_completed():
	get_tree().paused = false
