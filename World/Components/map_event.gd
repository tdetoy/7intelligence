extends Node2D

export(int) var trigger_id
export(Resource) var script_behavior
export(bool)var activated = true

var behavior

func init(caller):
	if($EventParent.get_children().size() != 0):
		behavior = $EventParent.get_children()[0]
	else:
		behavior = script_behavior.new()
	behavior.init(self, caller)
	if(!activated && behavior.is_inside_tree()):
		$Polygon2D.visible = false
		behavior.visible = false

func event_triggered(area):
	if(behavior != null && activated):
		behavior.perform(area)

func toggle_running():
	if(activated):
		activated = false
		if(behavior.is_inside_tree()):
			$Polygon2D.visible = false
			behavior.visible = false
	elif(!activated):
		activated = true
		if(behavior.is_inside_tree()):
			$Polygon2D.visible = true
			behavior.visible = true
