extends "spawner.gd"

signal teleport
signal new_trigger_target(object)

export(int) var pair_number = 0

var groups = []

func _ready():
	match direction:
		DIRECTION.SOUTH:
			$Appearance.frame = 49
		DIRECTION.NORTH:
			$Appearance.rotation_degrees += 90
			$Appearance.offset.y -= 16
		DIRECTION.EAST:
			$Appearance.rotation_degrees += 180
			$Appearance.offset = Vector2(-16, -16)
		DIRECTION.WEST:
			pass

func init(_map_handler, _map, _valid_cells, _obstacles, _actors, _props):
	map_handler = _map_handler
	connect("new_trigger_target", map_handler, "update_trigger_targets")
	
	for group in get_groups():
		if(group != "Spawner"):
			groups.append(group)
	
	for prop in _props:
		if(prop.is_in_group("Spawner") && prop != self && prop.pair_number == pair_number):
			var _drop = connect("teleport", prop, "spawn")

func despawn(_area):
	if(running && _area.owner.is_in_group("Minecart") && _area.owner != child):
		_area.owner.connect("move_complete", self, "finish_despawn")

func finish_despawn(emitter):
		emitter.queue_free()
		emit_signal("teleport")

func spawn():
	if(running):
		child = load(scene).instance()
		child.connect("ready", self, "init_child")
		map_handler.prop_parent.call_deferred("add_child", child)


func toggle_running():
	running = !running

func init_child():
	child.position = position
	child.init(map_handler,\
			   map_handler.map,\
			   map_handler.valid_cells, \
			   map_handler.obstacles, \
			   map_handler.actors, \
			   map_handler.props)
	for group in groups:
		child.add_to_group(group)
	emit_signal("new_trigger_target", child)
