extends Node2D

enum DIRECTION { NORTH, SOUTH, EAST, WEST }

export(DIRECTION) var direction = DIRECTION.WEST
export(bool) var running = true
export(String) var scene = ""
export(Array) var obstacle_exceptions = Array()

var map_handler
var child

func init(_map_handler, _map, _valid_cells, _obstacles, _actors, _props):
	pass

func spawn():
	pass

func despawn(_area):
	pass

func toggle_running():
	pass

func on_interact(_type, _gridpos, _sender):
	pass

func init_child():
	pass
