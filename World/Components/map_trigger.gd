#warnings-disable
#(just to get rid of that annoying "trigger_sent is not emitted" notice)

extends Node2D

signal trigger_sent(trigger, area)

export(bool) var map_goal
export(int) var trigger_id
export(bool) var activated = true

func init(_parent):
	if(!activated):
		$Polygon2D.visible = false

func _on_Area2D_area_entered(area):
	if(!area.owner.is_prop):
		emit_signal("trigger_sent", area)

func toggle_running():
	if(activated):
		activated = false
		$Polygon2D.visible = false
	elif(!activated):
		activated = true
		$Polygon2D.visible = true

func update_triggers(object):
	pass
