extends "map_trigger.gd"

export(String) var group_trigger = ""

signal toggle_sent(trigger, area)

var object_list = []

func init(_parent):
	if(group_trigger == ""):
		prints("Empty group trigger for", name)
		return
	for trigger in _parent.triggers:
		if(trigger.is_in_group(group_trigger)):
			var _drop0 = connect("toggle_sent", trigger, "toggle_running")
	for prop in _parent.props:
		if(prop.is_in_group(group_trigger)):
			var _drop1 = connect("toggle_sent", prop, "toggle_running")
	for event in _parent.events:
		if(event.is_in_group(group_trigger)):
			var _drop2 = connect("toggle_sent", event, "toggle_running")

func update_triggers(object):
	if(object.is_in_group(group_trigger)):
		var _drop3 = connect("toggle_sent", object, "toggle_running")

func _on_Area2D_area_entered(_area):
	if(activated):
		emit_signal("toggle_sent")
		$Sprite.flip_h = !$Sprite.flip_h
