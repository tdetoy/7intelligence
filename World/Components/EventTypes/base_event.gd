extends Node2D

var parent
var caller

func init(_parent, _caller):
	parent = _parent
	caller = _caller

func perform(_area):
	parent.get_node("Polygon2D").color = Color(0.0, 1.0, 0.0, 0.5)
