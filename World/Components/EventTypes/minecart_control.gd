extends "base_event.gd"

signal cart_toggle

var minecarts = []

func init(_parent, _caller):
	parent = _parent
	caller = _caller
	for prop in caller.props:
		if(prop.is_in_group("Minecart")):
			var _drop = connect("cart_toggle", prop, "toggle_running")

func perform(_area):
	emit_signal("cart_toggle")
	$Sprite.frame = wrapi($Sprite.frame + 1, 41, 43)
