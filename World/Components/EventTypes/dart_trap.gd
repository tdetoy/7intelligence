extends "base_event.gd"

enum DIRECTION { NORTH, SOUTH, EAST, WEST }

export(DIRECTION) var direction_point = DIRECTION.SOUTH

onready var look = $Sprite

var dart = load("res://Objects/Props/Dart.tscn")

var fired = false

func _ready():
	match direction_point:
		DIRECTION.SOUTH:
			pass
		DIRECTION.NORTH:
			$Sprite.rotation_degrees += 180
			$Sprite.position += Vector2(16, 16)
		DIRECTION.EAST:
			$Sprite.rotation_degrees -= 90
			$Sprite.position.y += 16
		DIRECTION.WEST:
			$Sprite.rotation_degrees += 90
			$Sprite.position.x += 16
		

func perform(_area):
	if(!fired):
		var dart_instance = dart.instance()
		call_deferred("add_child", dart_instance)
		yield(get_tree(), "idle_frame")
		dart_instance.global_position = global_position
		dart_instance.rotation_degrees = $Sprite.rotation_degrees
		dart_instance.start()
		fired = true
