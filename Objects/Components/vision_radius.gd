extends Area2D

func check():
	var result = get_overlapping_areas()
	if(result.size() > 0):
		return result
	return []
