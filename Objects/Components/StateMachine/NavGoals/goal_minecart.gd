extends Node

func determine_goal(_prev_state, machine):
	machine.target_type = "Crash"
	machine.change_state("Interact")
	var arr = []
	arr.append(Vector2(machine.body.gridpos.x + 1, machine.body.gridpos.y))
	arr.append(Vector2(machine.body.gridpos.x - 1, machine.body.gridpos.y))
	arr.append(Vector2(machine.body.gridpos.x, machine.body.gridpos.y + 1))
	arr.append(Vector2(machine.body.gridpos.x, machine.body.gridpos.y - 1))
	var result = machine.body.map_handler.query_map(arr, "Minecarts")
	if(result.size() == 1):
		machine.additional_arg = result[0]
		return ["Travel", "Travel", result[0]]
	else:
		for spot in result:
			if spot != machine.additional_arg:
				machine.additional_arg = machine.body.gridpos
				return ["Travel", "Travel", spot]
		return ["Process"]
