extends Node

#return should always be an array
#spot 0: next state to enter
#spot 1: state machine target type
#spot 2: target_gridpos
func determine_goal(_prev_state, machine):
	var in_range = machine.body.query_vision()
	for area in in_range:
		if(area.size() > 0):
			for result in area:
				var object = result.owner
				if(object.is_in_group("Knight")):
					if(object.is_fighting()):
						var next = machine.body.map.world_to_map(object.position - machine.body.map_handler.position)
						machine.body.nav.calc_path(next)
						if(next == machine.body.gridpos):
							return["Interact", "Hunt"]
						return ["Travel", "Hunt", next]
					else:
						var next = machine.body.gridpos
						var knight_pos = machine.body.map.world_to_map(object.position)
						if(knight_pos.x > next.x):
							next.x -= 1
						elif(knight_pos.x < next.x):
							next.x += 1
						if(knight_pos.y > next.y):
							next.y -= 1
						elif(knight_pos.y < next.y):
							next.y += 1
						if(machine.body.nav.is_valid_cell(next)):
							machine.body.nav.calc_path(next)
							return ["Travel", "Flee", next]
						else:
							next = machine.body.nav.get_closest(next)
							machine.body.nav.calc_path(next)
							return ["Travel", "Flee", next]
	return ["Process", "Idle"]
