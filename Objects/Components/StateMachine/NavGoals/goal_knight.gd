extends Node


func determine_goal(_prev_state, machine):
	var closest_object
	var goal
	var min_distance = 1000
	var in_range = machine.body.query_vision()
	var map_props = machine.body.map_handler.props
	for prop in map_props:
		if(!is_instance_valid(prop)):
			continue
		if(prop.is_in_group("Door") && prop.gridpos == machine.body.gridpos && !prop.open):
			machine.additional_arg = prop
			machine.target_type = "Locked Door"
			return["Interact"]
	for area in in_range:
		if(area.size() > 0):
			for result in area:
				if(result.owner != machine.body):
					var distance = machine.body.nav.calc_distance(machine.body.map.world_to_map(result.global_position))
					if(result.owner.is_in_group("Enemy")):
						if(distance > 0 && distance < min_distance || goal == "Treasure"):
							closest_object = result
							min_distance = distance
							goal = "Enemy"
							machine.additional_arg = result.owner
					elif(result.owner.is_in_group("Treasure") && goal != "Enemy"):
						if(distance > 0 && distance < min_distance):
							closest_object = result
							min_distance = distance
							goal = "Treasure"
	if(closest_object != null):
		var next = machine.body.map.world_to_map(closest_object.global_position - machine.body.map_handler.position)
		machine.body.nav.calc_path(next)
		machine.body.target_gridpos = next
		if(machine.target_type != "Goal" && machine.body.gridpos == machine.body.target_gridpos):
			return ["Interact"]
		return ["Travel", goal, next]
	elif(machine.body.gridpos == machine.body.target_gridpos):
		var next = machine.body.map_handler.get_next_map_goal()
		machine.target_type = "Goal"
		if(next != null):
			machine.body.target_gridpos = next
			machine.body.nav.calc_path(machine.body.target_gridpos)
			return ["Travel", "Goal", next]
		return ["Process"]
	else:
		return ["Travel", machine.target_type, Vector2.INF]

