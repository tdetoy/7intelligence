extends Node

signal interact(type, gridpos, actor)

var body
var target_type
var additional_arg

var state = []

var state_map = {}

func _ready():
	#populate state_map - allows slottable states into machine
	#give states their parent
	for child in get_children():
		state_map[child.name] = child
		child.init(self)


func init(_parent):
	body = _parent
	state.append("Idle")
	target_type = "Goal"
	change_state("Process")

func process(delta):
	body.text.text = state.front()
	state_map[state.front()].process(delta)

func input(event):
	state_map[state.front()].input(event)

func change_state(_state):
	var transition = state_map[state.front()].exit_state()
	if(!transition):
		return
	var prev_state = state.front()
	if(_state == "return"):
		state.pop_front()
	elif(_state == state.front()):
		return
	else:
		state.push_front(_state)
		state_map[state.front()].enter_state(prev_state)

func has_state(_state):
	return state_map.has(_state)

func reset():
	state.clear()
	state.append("Idle")
	target_type = "Goal"
	change_state("Process")
