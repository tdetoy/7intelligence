extends "base_state.gd"


func enter_state(_prev_state):
	if(machine.body.is_in_group("Enemy")):
		machine.body.life_timer.start()

func process(_delta):
	if(machine.body.is_in_group("Enemy") || machine.body.is_in_group("Door")):
		if(machine.body.life_timer.time_left < 10.0):
			if(machine.body.life_timer.time_left <= 0.3):
				emit_signal("done", "Dying")
				return
	if(!is_instance_valid(machine.additional_arg) || !machine.additional_arg.activated):
		machine.target_type = "Goal"
		emit_signal("done", "Process")
		return
	if(machine.additional_arg.gridpos != machine.body.gridpos):
		emit_signal("done", "Process")
		return
