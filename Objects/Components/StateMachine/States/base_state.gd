extends Node

signal done(next_signal)

var machine setget set_machine, get_machine

func init(_machine):
	set_machine(_machine)
	var _drop0 = connect("done", machine, "change_state")

func enter_state(_prev_state):
	pass

func exit_state():
	return true

func process(_delta):
	emit_signal("done", "Idle")

func input(_event):
	pass

func set_machine(_machine):
	machine = _machine

func get_machine():
	return machine
