extends "base_state.gd"

func process(_delta):
	if(machine.body.gridpos != machine.body.target_gridpos && !machine.body.moving):
		var next = machine.body.nav.get_move()
		if(next == Vector2.INF):
			emit_signal("done", "return")
			return
		
		machine.body.moving = true
		print(machine.body.global_position)
		print(next)
		print(machine.body.map.map_to_world(next) + machine.body.map_handler.global_position)
		#big ol command to start the tween
		machine.body.nav.nav_tween.interpolate_property(machine.body, \
		"global_position", machine.body.global_position, machine.body.map.map_to_world(next) + machine.body.map_handler.global_position, \
		machine.body.move_speed, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		
		machine.body.nav.nav_tween.start()

	elif (machine.body.gridpos == machine.body.target_gridpos && machine.body.nav.moves_left() == 0):
		match machine.target_type:
			"Treasure", "Enemy":
				emit_signal("done", "Interact")
			"Goal":
				machine.body.map_handler.register_goal_reached(machine.body.gridpos)
		emit_signal("done", "return")


func _on_NavTween_tween_all_completed():
	emit_signal("done", "return")
