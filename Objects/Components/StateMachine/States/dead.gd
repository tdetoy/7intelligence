extends "base_state.gd"

func enter_state(_prev_state):
	machine.body.on_dead()
	machine.body.visible = false
	machine.body.queue_free()
