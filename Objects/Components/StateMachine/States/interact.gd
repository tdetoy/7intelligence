extends "base_state.gd"

func enter_state(_prev_state):
	match machine.target_type:
		"Treasure":
			machine.emit_signal("interact", "Treasure", machine.body.gridpos, machine.body)
		"Enemy":
			machine.emit_signal("interact", "Fight", machine.body.gridpos, machine.body)
			emit_signal("done", "Fight")
			return
		"Locked Door":
			machine.emit_signal("interact", "Locked Door", machine.body.gridpos, machine.body)
			emit_signal("done", "Fight")
			return
		"Trap":
			machine.emit_signal("interact", "Trap", machine.body.gridpos, machine.body)
		"Hunt":
			#check if the knight is fighting, and if so, kill him
			machine.emit_signal("interact", "Hunt", machine.body.gridpos, machine.body)
		"Crash":
			machine.emit_signal("interact", "Crash", machine.body.gridpos, machine.body)
	machine.target_type = "Goal"
	emit_signal("done", "return")
