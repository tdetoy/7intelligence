extends "base_state.gd"

export(float) var dying_timer = 1.0

var timer
var dead = false


func enter_state(_prev_state):
	prints(machine.body.name, "entering dying")
	timer = dying_timer
	machine.body.valid_possess = false
	if(machine.body.possess.possessor != null):
		machine.body.possess.possessor.stop_possessing()
		machine.body.possess.possess_end()

func process(_delta):
	timer -= _delta
	if(timer <= 0.0):
		dead = true
		emit_signal("done", "Dead")

func exit_state():
	return dead
