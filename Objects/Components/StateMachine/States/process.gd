extends "base_state.gd"

var goals = []

func _ready():
	goals = get_children()

func enter_state(prev_state):
	var result_array = goals.front().determine_goal(prev_state, machine)
	if(result_array.size() > 1):
		machine.target_type = result_array[1]
		if(result_array.size() > 2):
			if(result_array[2] != Vector2.INF):
				machine.body.target_gridpos = result_array[2]
				machine.body.nav.calc_path(machine.body.target_gridpos)
	emit_signal("done", result_array[0])

func process(_delta):
	enter_state("return")
