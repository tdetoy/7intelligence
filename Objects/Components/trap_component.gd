extends Node

signal interact(type, gridpos, actor)

var material = load("res://Resources/Shaders/trap_glow.tres")

var body

func init(_body):
	body = _body
	body.material = material
	body.material.set_shader_param("trap_color", Preferences.trap_color)
	var actors = body.map_handler.actors
	for actor in actors:
		connect("interact", actor, "on_interact")

func activate():
	emit_signal("interact", "Trap", body.gridpos, body)
