extends Node

signal possess_update(object, status)

onready var possess_tween = $PossessTween

var body
var target_gridpos
var possessor

var active = true

func init(_parent):
	body = _parent
	body.add_to_group("PossessInit")
	var _drop0 = connect("possess_update", body, "possess_update")
	var _drop1 = possess_tween.connect("tween_all_completed", body, "move_complete")
	target_gridpos = body.gridpos

func process(_delta):
	if(body.is_in_group("Enemy")):
		if(body.state_machine.state.front() == "Fight" || body.state_machine.state.front() == "Dying"):
			body.state_machine.process(_delta)
	if(body.target_gridpos != body.gridpos && !body.moving):
		possess_tween.interpolate_property(body, "position",\
		body.position, body.map.map_to_world(body.target_gridpos),\
		0.15, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		body.moving = true
		possess_tween.start()

func input(_event):
	var direction = Vector2.ZERO
	direction.x = int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left"))
	direction.y = int(Input.is_action_pressed("ui_down")) - int(Input.is_action_pressed("ui_up"))

	if(body.nav.valid_cells.has(body.gridpos + direction) && !body.moving):
		body.target_gridpos = body.gridpos + direction

func possess_end():
	possessor = null
	if(!body.is_in_group("Knight")):
		body.return_brain()
	emit_signal("possess_update", null, false)

func possess_begin(actor):
	possessor = actor
	if(!body.is_in_group("Knight")):
		body.remove_brain(self)
	if(!body.moving):
		body.target_gridpos = body.gridpos
	emit_signal("possess_update", actor, true)

func disable():
	active = false

func highlight(_state):
#	body.anim_glow.stop()
#	if(state):
#		body.anim_glow.play("Glow")
#	elif(!state):
#		body.anim_glow.play("GlowStop")
#		if(possessor != null):
#			body.anim_glow.play("Possessed")
	pass
