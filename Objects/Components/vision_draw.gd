extends Node2D

var parent

class AngleSorter:
	static func angle_sort(a, b):
		return true if a.angle() >= b.angle() else false

func init(_parent):
	parent = _parent

func _draw():
	var results = parent.query_vision()
	var points = []
	var tracker = 0
	for result in results:
		if(result.size() != 0 && result[result.size() - 1].is_in_group("Blocker")):
			var set = determine_closest_pixel(result[result.size() - 1], (2 * PI / parent.vision_queries) * tracker)
			for point in set:
				if(point != Vector2.INF && !points.has(point) && to_global(point) != Vector2.ZERO && point):
					points.append(point)
#				else:
#					points.append(Vector2(0, parent.vision_radius).rotated((2 * PI / parent.vision_queries) * tracker) + Vector2(24, 24))
		else:
			points.append(Vector2(0, parent.vision_radius).rotated((2 * PI / parent.vision_queries) * tracker) + Vector2(24, 24))
#		if(tracker < points.size()):
#			draw_line(Vector2(24,24), points[tracker], Color(1.0, 0.0, 0.0, 0.2))
		tracker += 1
	if(points.size() > 3):
		points.sort_custom(AngleSorter, "angle_sort")
		draw_colored_polygon(points, Color(1.0, 0.0, 0.0, 0.2))

func determine_closest_pixel(area, rotate):
	#raycast out to bodies
	#use that to determine where next pixel should be 
	#breaks in polygon need to be investigated
	#iterate through n and n+1, checking if point lies between n and n+1
	#get angle of parent position to n and n+1 if valid
	#get results (either what they hit or maximum distance)
	#append to array in order of angle
	var set = []
	
	var vis_ray = RayCast2D.new()
	vis_ray.collide_with_areas = false
	vis_ray.collide_with_bodies = true
	parent.add_child(vis_ray)
	vis_ray.position = Vector2(24, 24)
	vis_ray.cast_to = Vector2(0, parent.vision_radius).rotated(rotate)
	vis_ray.force_raycast_update()
	
	var result = Vector2.INF
	
	if(vis_ray.get_collision_point() != null):
		if(vis_ray.get_collision_point().distance_to(parent.position) < parent.position.distance_to(Vector2(0, parent.vision_radius))):
			result = vis_ray.get_collision_point()
	vis_ray.queue_free()
	
	var poly = area.get_children()[0].shape.points
	
	for i in range(poly.size()):
		poly[i] = to_global(poly[i])
	
	for i in poly.size() - 1:
		if(is_on_line(poly[i], poly[i + 1], result)):
			if(parent.position.distance_to(poly[i]) < parent.position.distance_to(Vector2(0, parent.vision_radius))):
				set.append(to_local(poly[i]))
			else:
				var rot = poly[i].angle() + (PI / 2)
				set.append(Vector2(0, parent.vision_radius).rotated(rot))
			if(parent.position.distance_to(poly[i + 1]) < parent.position.distance_to(Vector2(0, parent.vision_radius))):
				set.append(to_local(poly[i + 1]))
			else:
				var rot = poly[i + 1].angle() + (PI / 2)
				set.append(Vector2(0, parent.vision_radius).rotated(rot))
		else:
			set.append(to_local(result))
	return set


#returns true if test exists on the line defined by endpoints a and b
func is_on_line(a, b, test):
	return true if(sqrt(pow((a.x - test.x),2) + pow((a.y - test.y),2)) + sqrt(pow((test.x - b.x),2) + pow((test.y - b.y),2)) == sqrt(pow((a.x - b.x),2) + pow((a.y - b.y),2))) else false
