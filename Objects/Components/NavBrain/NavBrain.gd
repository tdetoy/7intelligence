extends Node

onready var nav_tween = $NavTween

var pathfinding = AStar2D.new()
var valid_cells setget set_walkable, get_walkable
var obstacles setget set_obstacles, get_obstacles

var path = PoolVector2Array()

var body

func init(_actor, _walkable, _obstacles):
	body = _actor
	body.add_to_group("NavInit")
	var _drop0 = nav_tween.connect("tween_all_completed", body, "move_complete")
	set_obstacles(_obstacles)
	set_walkable(_walkable)

func calc_path(end_point):
	assert(valid_cells.has(body.gridpos))
	
	if(!valid_cells.has(end_point)):
#		print("what the shit")
		end_point = body.gridpos
	
	path.resize(0)
	
	var start_ind = valid_cells.find(body.gridpos)
	var end_ind = valid_cells.find(end_point)
	var result = pathfinding.get_id_path(start_ind, end_ind)
	
	for id in result:
		if(obstacles.has(valid_cells[id])):
			valid_cells.erase(id)
			reset()
			break
		if(valid_cells[id] != body.gridpos):
			path.append(valid_cells[id])
		

func calc_connections():
	for cell in valid_cells:
		if(valid_cells.has(Vector2(cell.x + 1, cell.y)) && !obstacles.has(Vector2(cell.x + 1, cell.y))):
			pathfinding.connect_points(valid_cells.find(cell), valid_cells.find(Vector2(cell.x + 1, cell.y)), false)
		if(valid_cells.has(Vector2(cell.x - 1, cell.y)) && !obstacles.has(Vector2(cell.x - 1, cell.y))):
			pathfinding.connect_points(valid_cells.find(cell), valid_cells.find(Vector2(cell.x - 1, cell.y)), false)
		if(valid_cells.has(Vector2(cell.x, cell.y + 1)) && !obstacles.has(Vector2(cell.x, cell.y + 1))):
			pathfinding.connect_points(valid_cells.find(cell), valid_cells.find(Vector2(cell.x, cell.y + 1)), false)
		if(valid_cells.has(Vector2(cell.x, cell.y - 1)) && !obstacles.has(Vector2(cell.x, cell.y - 1))):
			pathfinding.connect_points(valid_cells.find(cell), valid_cells.find(Vector2(cell.x, cell.y - 1)), false)

func set_walkable(_walkable):
	valid_cells = _walkable.keys()
	for cell in _walkable.keys():
		pathfinding.add_point(valid_cells.find(cell), Vector2(cell.x, cell.y), _walkable[cell])
	calc_connections()

func get_walkable():
	return valid_cells

func set_obstacles(_obstacles):
	obstacles = _obstacles

func get_obstacles():
	return obstacles

func reset():
	var points = pathfinding.get_points()
	var weights = {}
	for point in points:
		if(valid_cells.has(point)):
			weights[point] = pathfinding.get_weight_scale(valid_cells.find(point))
	pathfinding.clear()
	for cell in valid_cells:
		pathfinding.add_point(valid_cells.find(cell), Vector2(cell.x, cell.y), weights[cell] if weights.has(cell) else 1.0)
	calc_connections()

func get_move():
	if(path.size() == 0):
		return Vector2.INF
	else:
		if(obstacles.has(path[0])):
			calc_path(path[path.size() - 1])
			return Vector2.INF
		return manual_pop_front()

func moves_left():
	return path.size()


func manual_pop_front():
		path.invert()
		var ret_val = path[path.size() - 1]
		path.resize(path.size() - 1)
		path.invert()
		return ret_val

func calc_distance(end_point, start_point = body.gridpos):
	assert(valid_cells.has(start_point))
	
	if(!valid_cells.has(end_point)):
#		print("what the shit")
		end_point = body.gridpos
	
	var test_path = PoolVector2Array()
	
	var start_ind = valid_cells.find(start_point)
	var end_ind = valid_cells.find(end_point)
	var result = pathfinding.get_id_path(start_ind, end_ind)
	
	for id in result:
		if(obstacles.has(valid_cells[id])):
			return -1
		test_path.append(valid_cells[id])
		
	return test_path.size()

func is_valid_cell(cell):
	return (valid_cells.has(cell) && !obstacles.has(cell))

func get_closest(cell):
	var closest = pathfinding.get_closest_point(cell)
	return valid_cells[closest]

func update(_walkable, _obstacles):
	pathfinding.clear()
	set_obstacles(_obstacles)
	set_walkable(_walkable)
