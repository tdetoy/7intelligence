extends "actor.gd"

signal new_obstacle(gridpos, append)

var obstacle_gridpos = Vector2.INF

var running = true
var empty_brain

class EmptyBrain:
	func process(_delta):
		pass
	
	func input(_event):
		pass


func init_specific():
	empty_brain = EmptyBrain.new()
	var _drop = connect("new_obstacle", map_handler, "register_obstacle")


func toggle_running():
	if(running):
		remove_brain(empty_brain)
		running = false
		if(moving):
			obstacle_gridpos = target_gridpos
		else:
			obstacle_gridpos = gridpos
		emit_signal("new_obstacle", obstacle_gridpos, true)
	elif(!running):
		return_brain()
		running = true
		emit_signal("new_obstacle", obstacle_gridpos, false)
		obstacle_gridpos = Vector2.INF
