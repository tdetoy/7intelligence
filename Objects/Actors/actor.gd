extends Sprite

signal remove_self(actor)
signal move_complete(actor)

const is_prop = false

#the variable listed below can be thought of as a reciprocal
#actual move speed is 1/move_speed
export(float) var move_speed = 0.75
export(NodePath) var player_path = null
export(float) var vision_radius = 200.0
export(int) var vision_queries = 45
export(float) var life_time = 10.0

onready var nav = $NavBrain
onready var possess = $Possessable
onready var state_machine = $StateMachine
onready var text = $State
onready var pos_text = $Gridpos
onready var anim_glow = $AnimationGlow
onready var body_area = $BodyArea
onready var life_timer = $LifeTimer
onready var vision_parent = $VisionAreaParent
onready var vision_draw = $VisionDraw

var valid_possess = true
var player
var map_handler
var map
var gridpos
var target_gridpos
var moving = false
var motivator
var attached_trap
var trap_activated = true
var initialized = false
var activated = false

func init(_handler, _map, _cell_list, _obstacle, _actors, _props):
	motivator = state_machine
	map_handler = _handler
	map = _map
	gridpos = map.world_to_map(position)
	target_gridpos = gridpos
	#50.0 is the radius of the mask in pixels, to reflect vision radii
	vision_draw.texture_scale = vision_radius / 50.0
	if(nav != null):
		nav.init(self, _cell_list, _obstacle)
	if(possess != null):
		possess.init(self)
	if(state_machine != null):
		state_machine.init(self)
	for actor in _actors:
		state_machine.connect("interact", actor, "on_interact")
	for prop in _props:
		state_machine.connect("interact", prop, "on_interact")
	body_area.owner = self
	attached_trap = find_node("TrapComponent")
	if(attached_trap != null):
		attached_trap.init(self)
	connect("remove_self", map_handler, "remove_object")
	initialized = true
	activated = true
	init_specific()

func reinit(_handler, _map, _cell_list, _obstacle, _actors, _props):
	map_handler = _handler
	map = _map
	gridpos = map.world_to_map(position)
	target_gridpos = gridpos
	
	var obstacles = Array()
	
	for pos in _obstacle.keys():
		if (_obstacle[pos].empty()):
			obstacles.append(pos)
		else:
			var exempt = false
			for group in _obstacle[pos]:
				if is_in_group(group):
					exempt = true
			if(!exempt):
				obstacles.append(pos)
	
	nav.update(_cell_list, obstacles)
	get_node(player_path).reset()
	for actor in _actors:
		state_machine.connect("interact", actor, "on_interact")
	for prop in _props:
		state_machine.connect("interact", prop, "on_interact")

func init_specific():
	pass

func _process(delta):
	if(activated):
		motivator.process(delta)

func _physics_process(_delta):
	if(activated):
		query_vision()

func _input(event):
	if(activated):
		motivator.input(event)

func click(incoming_gridpos):
	nav.calc_path(incoming_gridpos)

func possess_update(_body, _state):
	pass

func move_complete():
	emit_signal("move_complete", self)
	gridpos = map.world_to_map(position)
	pos_text.text = str(gridpos)
	moving = false

func on_interact(_type, _gridpos, _sender):
	pass

func on_dead():
	emit_signal("remove_self", self)
	on_dead_specific()

func on_dead_specific():
	pass

func setup_vision_radius():
	for i in range(vision_queries):
		var area = Area2D.new()
		vision_parent.add_child(area)
		var shape = CollisionShape2D.new()
		area.add_child(shape)
		area.position = Vector2(8,8)
		area.monitoring = true
		area.monitorable = false
		shape.shape = SegmentShape2D.new()
		shape.shape.a = Vector2(0,0)
		shape.shape.b = Vector2(0, vision_radius)
		area.rotation += ((2 * PI) / vision_queries) * i

func query_vision():
	var vis_arr = []
	for area in vision_parent.get_children():
		var result = area.get_overlapping_areas()
		if(result.size() == 1):
			result.remove(0)
		elif(result.size() > 1):
			result = distance_sort(result)
		for i in range(result.size()):
			if(result[i].is_in_group("Blocker")):
				result.resize(i + 1)
				break
		vis_arr.append(result)
	return vis_arr

func distance_sort(vec_arr):
	if(vec_arr.size() <= 1):
		return vec_arr
	var flipped = true
	while(flipped):
		flipped = false
		for i in range(0, vec_arr.size() - 1):
			if(vec_arr[i].global_position.distance_to(global_position) > vec_arr[i + 1].global_position.distance_to(global_position)):
				flipped = true
				var temp = vec_arr[i]
				vec_arr[i] = vec_arr[i + 1]
				vec_arr[i + 1] = temp
	return vec_arr

func set_life_timer():
	life_timer.set_wait_time(life_time)

func interact_end():
	life_timer.pause()

func is_fighting():
	return(state_machine.state.front() == "Fight")

func remove_brain(new_brain):
	motivator = new_brain

func return_brain():
	motivator = state_machine

func update_obstacles(obstacle_list):
	if(nav != null):
		nav.obstacles = obstacle_list

func toggle_running():
	if(trap_activated):
		trap_activated = false
	elif(!trap_activated):
		trap_activated = true

func update_nav(_cell_list, _obstacles):
	if(nav != null):
		nav.update(_cell_list, _obstacles)

func deactivate():
	activated = false

func activate():
	activated = true

func reset():
	state_machine.reset()
	visible = true

func update_position():
	gridpos = map.world_to_map(position)
