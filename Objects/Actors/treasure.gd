extends "actor.gd"

func _ready():
	pass

func init_specific():
	$VisionDraw.visible = false

func on_interact(_type, _gridpos, _sender):
	if(gridpos == _gridpos && _sender != self):
		emit_signal("remove_self", self)
		if(possess.possessor != null):
			possess.possessor.stop_possessing()
		if(attached_trap != null && trap_activated):
			attached_trap.activate()
		queue_free()
