extends "actor.gd"

#doors should not be obstacles for pathfinding purposes (unless they're metal)
#however, doors should definitely increase the weight of a given point on the
#graph, bringing it back to 1 when they're destroyed or opened
#good lord there have been some issues with progress, thank god for the git

signal door_update(gridpos, weight)

export(bool) var openable = true
export(bool) var open = false


enum DOOR_TYPE { WOOD, METAL }

export(DOOR_TYPE) var type = DOOR_TYPE.WOOD
export(float) var door_weight = 10.0

func init_specific():
	connect("door_update", map_handler, "update_map_weights")
	emit_signal("door_update", gridpos, door_weight)
	if(open):
		#this is stupid but at least it aligns with what i'm attempting to do
		#without any bespoke code blurgh
		open = !open
		toggle_running()

func on_dead_specific():
	if(attached_trap != null &&  trap_activated):
		attached_trap.activate()
	emit_signal("door_update", gridpos, 1.0)

func on_interact(_type, _gridpos, _sender):
	if(_type == "Locked Door" && !open && life_timer.is_stopped() && gridpos == _gridpos):
		state_machine.additional_arg = _sender
		state_machine.change_state("Fight")
		life_timer.start()

func toggle_running():
	if(openable):
		if(!open):
			emit_signal("door_update", gridpos, 1.0)
			frame = 5
			trap_activated = false
			open = true
		elif(open):
			emit_signal("door_update", gridpos, door_weight)
			frame = 4
			trap_activated = true
			open = false
	elif(!openable):
		if(trap_activated):
			trap_activated = false
		elif(!trap_activated):
			trap_activated = true
		
