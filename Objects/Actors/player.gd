extends Node2D


onready var particles = $Particles2D
onready var anim = $AnimationPlayer

var state = Array()

enum STATES { PICKING, POSSESSING }


var possible_targets = Array()
var checking = false
var possess_target
var currently_possessing
var squire

func set_squire(_squire):
	squire = _squire
	currently_possessing = squire
	state.push_front(STATES.POSSESSING)

func _process(delta):
	match state.front():
		STATES.PICKING:
			process_assessing(delta)
		STATES.POSSESSING:
			process_possessing(delta)

func _unhandled_input(event):

	match state.front():
		STATES.PICKING:
			input_assessing(event)
		STATES.POSSESSING:
			input_possessing(event)

func process_assessing(_delta):
	#undecided on camera's involvement
#	if(is_instance_valid(possess_target)):
#		camera.global_position = possess_target.global_position + Vector2(24, 24)
#	elif(is_instance_valid(currently_possessing)):
#		camera.global_position = currently_possessing.global_position + Vector2(24, 24)
#	else:
#		camera.global_position = squire.global_position + Vector2(24, 24)
	possible_targets.clear()
	for area in $Area2D.get_overlapping_areas():
		if(area.owner != possess_target && is_instance_valid(area.owner) && area.owner.is_in_group("Possessable")):
			if(area.owner.possess.active && area.owner.valid_possess && area.owner.activated):
				area.owner.possess.highlight(true)
				possible_targets.append(area.owner)
				print(area.owner.name)
	if(possess_target != squire):
		squire.possess.highlight(true)
		possible_targets.append(squire)

func process_possessing(_delta):
	#similarly, undecided on camera
#	camera.global_position = currently_possessing.global_position + Vector2(24, 24)
#	if(!is_instance_valid(currently_possessing)):
#		stop_possessing()
	if(!is_instance_valid(currently_possessing)):
		stop_possessing()

func input_assessing(event):
	if(Input.is_action_just_released("ui_accept")):
		set_effects(false)
		state.pop_front()
		if(is_instance_valid(possess_target)):
			currently_possessing = possess_target
			currently_possessing.possess.possess_begin(self)
		for target in possible_targets:
			if(is_instance_valid(target)):
				target.possess.highlight(false)
		possible_targets.clear()
		return
	if(possible_targets.size() == 0):
		possess_target = currently_possessing
		return
		#^ ??? why does this exist
	determine_next_target(event)

func input_possessing(event):
	if(Input.is_action_just_pressed("ui_accept")):
		set_effects(true)
		currently_possessing.possess.possess_end()
		state.push_front(STATES.PICKING)
		return
	currently_possessing._input(event)

func stop_possessing():
	currently_possessing = squire

func set_effects(_state):
	if(_state):
		Engine.time_scale = 0.2
		particles.visible = true
		anim.play("Particles")
	elif(!_state):
		Engine.time_scale = 1.0
		particles.visible = false
		particles.process_material.emission_sphere_radius = 0.0
	#TODO - determine effects for possess picking state

func sort_distances(arr):
	var repeat = true
	while repeat:
		var ind = 0
		repeat = false
		while ind < arr.size() - 1:
			var calc1 = squire.nav.calc_distance(arr[ind].gridpos, currently_possessing.gridpos)
			var calc2 = squire.nav.calc_distance(arr[ind + 1].gridpos, currently_possessing.gridpos)
			if calc1 > calc2:
				var temp = arr[ind]
				arr[ind] = arr[ind + 1]
				arr[ind + 1] = temp
				repeat = true
			ind += 1
	return arr

func determine_next_target(_event):
	var temp_arr = Array()
	if(Input.is_action_just_pressed("ui_left")):
		for target in possible_targets:
			if target.gridpos.x < currently_possessing.gridpos.x:
				temp_arr.append(target)
	elif(Input.is_action_just_pressed("ui_right")):
		for target in possible_targets:
			if target.gridpos.x > currently_possessing.gridpos.x:
				temp_arr.append(target)
	elif(Input.is_action_just_pressed("ui_up")):
		for target in possible_targets:
			if target.gridpos.y < currently_possessing.gridpos.y:
				temp_arr.append(target)
	elif(Input.is_action_just_pressed("ui_down")):
		for target in possible_targets:
			if target.gridpos.y > currently_possessing.gridpos.y:
				temp_arr.append(target)
	
	if(temp_arr.size() > 1):
		temp_arr = sort_distances(temp_arr)
	if(temp_arr.size() > 0):
		possess_target = temp_arr[0]

func reset():
	currently_possessing = squire
