extends "actor.gd"

func init_specific():
	set_life_timer()
	setup_vision_radius()
	vision_draw.texture_scale = vision_radius / 50.0

func on_interact(_type, _gridpos, _sender):
	if(_type == "Fight" && life_timer.is_stopped() && gridpos == _gridpos):
		state_machine.additional_arg = _sender
		state_machine.change_state("Fight")
		life_timer.start()
	if(_type == "Crash" && gridpos == _gridpos):
		state_machine.change_state("Dying")
