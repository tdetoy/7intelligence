extends "actor.gd"

func init_specific():
	setup_vision_radius()
	vision_draw.texture_scale = vision_radius / 50.0
	$Player.set_squire(self)

func on_interact(_type, _gridpos, _sender):
	if((_type == "Hunt" || _type == "Crash" || _type == "Trap") && gridpos == _gridpos && _sender != self):
		state_machine.change_state("Dying")
