extends Node2D

export(int) var speed = 60
export(Array) var obstacle_exceptions = Array()

const is_prop = true

var active = false


func _process(delta):
	if(active):
		position += Vector2(0, 1).rotated(rotation) * speed * delta

func start():
	$Sprite.rotation_degrees += 90
	if($Sprite.rotation_degrees + rotation_degrees== 90):
		position.x += 16
	elif($Sprite.rotation_degrees + rotation_degrees == 180):
		position += Vector2(16, 16)
	elif($Sprite.rotation_degrees +rotation_degrees == 270):
		position.y += 16
	active = true


func _on_Area2D_area_shape_entered(_area_id, _area, _area_shape, _self_shape):
	var target = _area.owner
	prints("hit:", target)
	if(!target.is_in_group("Living")):
		queue_free()
		return
	if(_area.owner.state_machine.has_state("Dying")):
		_area.owner.state_machine.change_state("Dying")
	queue_free()
